package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("BMI");
        Scene scene = new Scene(root, 600, 400);
        scene.getStylesheets().add("style.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @FXML
    private Label labelFour;

    @FXML
    private TextField textFieldOne, textFieldTwo;

    @FXML
    protected void onClick(ActionEvent event) {
        double textField1 = Double.parseDouble(textFieldOne.getText());
        double textField2 = Double.parseDouble(textFieldTwo.getText());
        double textFiled3 = Math.round(textField1 / Math.pow(textField2, 2) * 100.0) / 100.0;
        //labelFour.setText(Double.toString(textFiled3));

        if (textFiled3 < 18.5) {
            labelFour.setText(textFiled3 + " " + "Niedowaga");
        }
        if (textFiled3 >= 18.5 && textFiled3 <= 24.9) {
            labelFour.setText(textFiled3 + " " + "Norma");
        }
        if (textFiled3 > 24.9 && textFiled3 <= 29.9) {
            labelFour.setText(textFiled3 + " " + "Nadwaga");
        }
        if (textFiled3 > 30.0) {
            labelFour.setText(textFiled3 + " " + "Otyłość!!!");
        }
    }
}
